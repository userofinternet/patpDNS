import hashlib
import sys
import sqlite3

def sha256sum(filename):
    with open(filename, 'rb', buffering=0) as f:
        consensus_hash = "9f513f1ceadb6a01c5485b7dbdfd5118dc66cd70b59cae2851292112d4066a32"
        dict_hash = hashlib.file_digest(f, 'sha256').hexdigest()
        if dict_hash != consensus_hash:
            sys.exit("\nThe dictionary hash doesn't match the consensus hash.")

def read_wordlist_from_file(filename):
    with open(filename, 'r') as file:
        wordlist = [line.strip() for line in file if line.strip().islower()]

    return wordlist

def sha256_to_pat(input_string, wordlist):
    # Generate SHA-256 hash
    hash_object = hashlib.sha256(input_string.encode())
    hash_value = hash_object.digest()

    # Convert the hash value to a 64-bit integer
    hash_int = int.from_bytes(hash_value, byteorder='big')

    # Divide the hash integer into three sections
    section1 = hash_int % len(wordlist)
    section2 = hash_int // len(wordlist) % len(wordlist)
    section3 = hash_int // len(wordlist) // len(wordlist) % len(wordlist)

    # Map the sections to syllables
    syllable1 = wordlist[section1]
    syllable2 = wordlist[section2]
    syllable3 = wordlist[section3]

    # Create patp (planet identifier) with original string
    patp = f"{input_string}|{syllable1[:3]}{syllable2[:3]}{syllable3[:3]}"

    return patp

# Check if the script is invoked with a command-line argument
if len(sys.argv) > 1:
    input_string = sys.argv[1]
    wordlist_file = '/usr/share/dict/american-english'
    
    # Check hash of dict against consensus hash
    sha256sum(wordlist_file)

    # Read wordlist from file
    wordlist = read_wordlist_from_file(wordlist_file)

    # Generate patp
    patp_result = sha256_to_pat(input_string, wordlist)
    host1 = patp_result.split('|')[1]
    host2 = host1 + '.wan'

    # Extract the original string from patp
    original_string = patp_result.split('|')[0]

    print("Original String:", original_string)
    print("Generated Patp:", patp_result)

    # Connect to the SQLite3 database
    conn = sqlite3.connect('dns_records.db')
    c = conn.cursor()

    # Create a table if it doesn't exist
    c.execute('''CREATE TABLE IF NOT EXISTS dns_records (hostname TEXT, ip_address TEXT, record_type TEXT)''')

    # Determine the record type based on the input string
    record_type = "A" if ':' not in input_string else "AAAA"

    # Insert the record into the database
    c.execute("INSERT INTO dns_records (hostname, ip_address, record_type) VALUES (?, ?, ?)",
              (host2, input_string, record_type))

    # Commit the changes and close the connection
    conn.commit()
    conn.close()

else:
    print("Please provide a string as a command-line argument.")



