import socket

def read_dns_records(file_path):
    records = {}
    with open(file_path, 'r') as file:
        for line in file:
            if line.strip():
                ip_address, hostname = line.strip().split('|')
                records[hostname] = ip_address
    return records


def handle_dns_request(data, records):
    hostname = data.decode().strip()
    if hostname in records:
        ip_address = records[hostname]
        response = ip_address.encode()
    else:
        response = b'No record found'
    return response

def main():

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('127.0.0.1', 53))

    print('DNS server is running...')

    while True:
        data, addr = server_socket.recvfrom(1024)
        print('Received DNS request from:', addr)
        
        records = read_dns_records('patp-records.txt')

        response = handle_dns_request(data, records)

        server_socket.sendto(response, addr)
        print('Sent DNS response to:', addr)

if __name__ == '__main__':
    main()

