import socket

def send_dns_request(hostname):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    client_socket.sendto(hostname.encode(), ('127.0.0.1', 53))
    response, _ = client_socket.recvfrom(1024)
    print('Received DNS response:', response.decode())

def main():
    hostname = input('Enter the hostname to lookup: ')
    send_dns_request(hostname)

if __name__ == '__main__':
    main()

