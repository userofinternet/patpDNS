# patpDNS
patpDNS is an attempt to create an alternative to traditional DNS. This may or may not have been attempted before, but i'm creating this repo just to host and example and brainstorm with others on the idea.

the idea is that you can create a hash of an ip address and then represent that hash via a patp or BIP39 method to create short memorable names. These names can then be stored in a text file on the users machine along with the coresponding ip the resolve to. Consensus is achieved by everyone simply agreeing on one dictionairy file of words to be used as the source for the patp or BIP39 method. for the time being the default american english dictionairy file that is included in Ubuntu Linux is used as the consensus dictionairy file as an example.

There are 3 python scripts here so far. one is a dns server script that will serve the patp dns records from the local text file to the users machine via localhost. there is also a basic dns client script for testing the server script. and finally there is the patpDNS script that does the magic.

## Usage
python3 patpdns.py 192.168.1.1
